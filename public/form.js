$('document').ready(function(){
    $('.pi').on('click', function(e){
        e.preventDefault();
        let href = $(this).attr('href');
        getContent(href, true);
    });

    $('.close').click(function(){
         $('.zv_up').fadeOut();
         $('main').css('filter','none');
         window.history.back();
      });
});

window.addEventListener("popstate", function(e) {
    getContent(location.pathname, false);
});


function getContent(url, addEntry) {
        if(addEntry === true) {
            window.requestAnimationFrame(show);
         $('.zv_up').fadeIn();
         $('main').css('filter','blur(5px)');
         window.history.pushState({page:1}, null, '?#form_zvon');}
         else {
             $('.zv_up').fadeOut();
             $('main').css('filter','none');
             window.history.replaceState({page:0},null,'https://dperchenko.gitlab.io/web10/');
         }
        }

function animate(options) {

  let start = performance.now();

  requestAnimationFrame(function animate(time) {

    let timeFraction = (time - start) / options.duration;
    if (timeFraction > 30) timeFraction = 30;


    let progress = formingTiming(1.5, timeFraction);
    
    options.draw(progress);

    if (timeFraction < 30) {
      requestAnimationFrame(animate);
    }

  });
}

function formingTiming(x, timeFraction) {
  return Math.pow(timeFraction, 2) * ((x + 1) * timeFraction - 8*x)
}

function linear(timeFraction) {
  return timeFraction;
}
function show() { {
    form.style.display = "inline";
  
    animate({
      duration: 200,
      timing: formingTiming(1.5, linear),
      draw(progress) {
        if(progress < 1) {form.style.top = progress;}
        else {form.style.top = 70 + '%';}
      }
    });
  };
}

